(setq package-selected-packages '(magit vertico orderless gruvbox-theme))

;; global modes
(vertico-mode)
(global-display-line-numbers-mode 1)
(global-visual-line-mode 1)
(menu-bar-mode 0)
(scroll-bar-mode 0)
(tool-bar-mode 0)

;; hooks
(add-hook 'prog-mode-hook 'electric-pair-mode)
(add-hook 'org-mode-hook 'flyspell-mode)
(add-hook 'org-mode-hook 'auto-fill-mode)
(add-hook 'org-mode-hook 'org-indent-mode)

;; settings
(setq inhibit-startup-screen t
      initial-scratch-message nil
      org-pretty-entities t
      completion-styles '(orderless basic)
      completion-category-overrides '((file (styles basic partial-completion)))
      confirm-kill-emacs 'yes-or-no-p
      delete-by-moving-to-trash t
      visible-bell t		   
      disabled-command-function nil
      auto-save-file-name-transforms `((".*" "~/.config/emacs/backups/" t)) ;; backup and autosave
      backup-directory-alist `(("." . "~/.config/emacs/backups/"))
      kept-new-versions 6
      kept-old-versions 2
      delete-old-version t)

(setq-default fill-column 66
	      message-log-max nil)

;; keybindings
(define-prefix-command 'coding-map)
(define-prefix-command 'toggle-map)

(global-set-key (kbd "C-c c") 'coding-map)
(global-set-key (kbd "C-c t") 'toggle-map)
(global-set-key (kbd "C-c w") 'ispell-word)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key [remap save-buffers-kill-terminal] #'save-buffers-kill-emacs)

(define-key coding-map (kbd "w") 'whitespace-cleanup)
(define-key coding-map (kbd "W") 'whitespace-mode)
(define-key coding-map (kbd "r") 'eglot-rename)
(define-key toggle-map (kbd "l") 'display-line-numbers-mode)
(define-key toggle-map (kbd "v") 'visual-line-mode)
(define-key toggle-map (kbd "i") 'org-indent-mode)
(define-key toggle-map (kbd "e") 'eglot)
(define-key completion-in-region-mode-map (kbd "M-n") 'minibuffer-next-completion)
(define-key completion-in-region-mode-map (kbd "M-p") 'minibuffer-previous-completion)
(with-eval-after-load 'ibuffer
  (define-key ibuffer-mode-map (kbd "q") 'kill-this-buffer))

(kill-buffer "*Messages*")
(load-theme 'gruvbox t)
(add-to-list 'default-frame-alist '(font . "Fira Code-10"))
