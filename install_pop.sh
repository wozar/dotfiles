#!/bin/bash

# my install script for Pop!_OS Linux

sudo apt install black discord fzf htop ncal neovim ripgrep syncthing texlive-latex-extra

mkdir ~/.config/emacs
rmdir Templates Public

# configs
cp ~/Documents/dotfiles/.config/emacs/init.el ~/.config/emacs/init.el

# fzf
echo "source /usr/share/doc/fzf/examples/key-bindings.bash" >> ~/.bashrc

# show weeks in notification window
gsettings set org.gnome.desktop.calendar show-weekdate true
