#!bin/bash

sudo apt build-dep emacs
sudo apt install libgccjit-11-dev libjansson-dev
git clone git://git.savannah.gnu.org/emacs.git ~/.local/emacs/
(cd ~/.local/emacs/ && ./autogen.sh)
(cd ~/.local/emacs/ && ./configure --with-json --with-native-compilation --prefix=/home/$USER/.local/)
(cd ~/.local/emacs/ && make)
(cd ~/.local/emacs/ && make install)
#make -f ~/.local/emacs/Makefile
#make install -f ~/.local/emacs/Makefile
